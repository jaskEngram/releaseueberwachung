@file: JvmName("MainKt")

package de.engram.isp.releaseueberwachung

import com.opencsv.CSVReaderBuilder
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import com.opencsv.CSVParserBuilder
import org.jsoup.Jsoup

data class ProcessInfo(val name: String, val documentation: String?, val calledFrom: Set<String>?)

class ProcessLists {
    companion object {
        val changedProcesses = mutableMapOf<String, String>()      // name, documentation
        val usedProcesses = mutableMapOf<String, Set<String>>()    // name, calledFrom
        val processInfos = mutableSetOf<ProcessInfo>()
    }
}

class HtmlConstants {
    companion object {
        val prefixTemplate = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"> " +
                "<html lang=\"en\"> <head> <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"> " +
                "<title>%s</title> </head> <body>"
        val suffix = "</body> </html>"
        val openHeading = "<h1>"
        val closeHeading = "</h1>"
        val openSubHeading = "<h2>"
        val closeSubHeading = "</h2>"
        val openUnorderedList  = "<ul>"
        val closeUnorderedList = "</ul>"
        val openListElement = "<li>"
        val closeListElement = "</li>"
        val openTable = "<table>"
        val closeTable = "</table>"
    }
}

fun main(args: Array<String>) {
    if (args.size < 3) {
        println("Bitte das Skript mit drei Pfaden als Argumente aufrufen:")
        println("Der erste Pfad zeigt auf die htm Seite mit der Änderungshistorie des Releases.")
        println("Der zweite Pfad zeigt auf den Export des pmsparser.")
        println("Der dritte Pfad gibt den gewünschten Pfad für die Ergebnisdatei an.")
        println("Beispielaufruf: java -jar releaseueberwachung.jar \"./Aenderungshistorie-Prozessgruppen.htm\" " +
                "\"./export.csv\" \"./result.html\"")
        println("Es ist wichtig, korrekte Pfade anzugeben, ob fully qualified oder relative. Bspw. \"./result.html\" " +
                "anstatt \"result.html\"")
        return
    }

    val changedProccesesFile = File(args[0])
    if (changedProccesesFile.exists()) {
        parseChangedProceesses(changedProccesesFile)
    } else {
        println("Fehler: File ${changedProccesesFile} existiert nicht")
        return
    }

    val usedProcessesFile = File(args[1])
    if (usedProcessesFile.exists()) {
        parseUsedProcceses(usedProcessesFile)
    } else {
        println("Fehler: File ${usedProcessesFile} existiert nicht")
        return
    }

    val intersectionProcesses = ProcessLists.changedProcesses.keys
            .intersect(ProcessLists.usedProcesses.keys);
    for (processName in intersectionProcesses) {
        val documentation = ProcessLists.changedProcesses.get(processName)
        val calledFrom = ProcessLists.usedProcesses.get(processName)
        ProcessLists.processInfos.add(ProcessInfo(processName, documentation, calledFrom))
    }

    val resultHtmlFile = File(args[2])
    resultHtmlFile.parentFile.mkdirs()
    writeResultHtmlFile(resultHtmlFile)
    println("Prozess erfolgreich beendet")
}


fun parseChangedProceesses(file: File) {
    val doc = Jsoup.parse(file, "windows-1252")
    val content = doc.body().select("h4,table")
    val it = content.listIterator()

    while (it.hasNext()) {
        val processName: String
        var documentation = "" // documentation may be empty, name can't
        val headingElement = it.next()
        if (!headingElement.tagName().equals("h4")) {
            println("Fehler: Erwartete h4 Element, aber fand ${headingElement.tagName()}")
            continue
        }
        processName = headingElement.text().substringAfter("Prozessname:").trim()
        while (it.hasNext()) {
            val tableElement = it.next()
            if (!tableElement.tagName().equals("table")) {
                it.previous()
                break
            }
            documentation += tableElement.html()
        }
        ProcessLists.changedProcesses.put(processName, documentation)
    }
}

fun parseUsedProcceses(file: File) {
    val parser = CSVParserBuilder()
            .withSeparator(';')
            .withIgnoreQuotations(true)
            .build()
    val fileReader = BufferedReader(FileReader(file))
    val csvReader = CSVReaderBuilder(fileReader)
            .withCSVParser(parser)
            .build()

    val records = csvReader.readAll()
    for (record in records) {
        var processName = record.first()                                 // get first column containing the process name
                ?.split('#')?.first()                         // remove DynS version suffix #N
                ?.replace(Regex("[^ -~]"), "")        // Remove control characters
                ?.replace(Regex("[^\\x00-\\x7F]"), "")// Remove non-ascii characters
        if (processName != null) {
            if (Regex(".*[A-Z]{1}[0-9]{2}$").matches(processName)) {
                val oldProcessName = processName
                processName = StringBuilder(processName).insert(processName.length-2, "_").toString();
                println("Passe genutzten Prozessnamen von ${oldProcessName} zu ${processName} an")
            }
            val calledFrom = HashSet(record.takeLast(record.size - 1))
            ProcessLists.usedProcesses.put(processName, calledFrom)
        } else {
            println("Fehler: Invalide CSV Zeile ${record}")
        }
    }
    csvReader.close()
}

fun writeResultHtmlFile(file: File) {
    val stringBuilder = StringBuilder()
    val prefixWithTitle = String.format(HtmlConstants.prefixTemplate, file.name.substringBeforeLast('.'))
    stringBuilder.append(prefixWithTitle)
    for (processInfo in ProcessLists.processInfos) {
        stringBuilder.append(HtmlConstants.openHeading)
        stringBuilder.append(processInfo.name)
        stringBuilder.append(HtmlConstants.closeHeading)

        stringBuilder.append(HtmlConstants.openSubHeading)
        stringBuilder.append("Beschreibung")
        stringBuilder.append(HtmlConstants.closeSubHeading)
        stringBuilder.append(HtmlConstants.openTable)
        stringBuilder.append(processInfo.documentation)
        stringBuilder.append(HtmlConstants.closeTable)

        stringBuilder.append(HtmlConstants.openSubHeading)
        stringBuilder.append("Aufgerufen aus...")
        stringBuilder.append(HtmlConstants.closeSubHeading)
        stringBuilder.append(HtmlConstants.openUnorderedList)
        for (caller in processInfo.calledFrom.orEmpty()) {
            stringBuilder.append(HtmlConstants.openListElement)
            stringBuilder.append(caller)
            stringBuilder.append(HtmlConstants.closeListElement)
        }
        stringBuilder.append(HtmlConstants.closeUnorderedList)
    }
    stringBuilder.append(HtmlConstants.suffix)

    file.writeText(stringBuilder.toString())
}