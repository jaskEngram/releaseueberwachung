Bitte das Skript mit drei Pfaden als Argumente aufrufen:

Der erste Pfad zeigt auf die htm Seite mit der Änderungshistorie des Releases.

Der zweite Pfad zeigt auf den Export des pmsparser.

Der dritte Pfad gibt den gewünschten Pfad für die Ergebnisdatei an.

Beispielaufruf: java -jar releaseueberwachung.jar "./Aenderungshistorie-Prozessgruppen.htm" "./export.csv" "./result.html"

Es ist wichtig, korrekte Pfade anzugeben, ob fully qualified oder relative. Bspw. "./result.html" anstatt "result.html"